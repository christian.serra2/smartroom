Io (Maicol Forti) e Christian Serra abbiamo optato per una semplice soluzione basata sul modello ad attori per risolvere il problema proposto.
L'attore RoomTwin rappresenta il digital twin della stanza. (In modo randomico cambia la propria temperatura per simulare una situazione reale).
L'attore Thermostat rappresenta il termostato. (Attraverso il termostato viene definita una temperatura da raggiungere e si manipola la stanza fino ad ottenerla).
L'attore Thermostat2 rappresenta il termostato della seconda versione. (Come il primo ma mantiene la temperatura).
Questi attori Thermostat cercano di utilizzare un approccio Sense, Plan, Act. 
Ottenengono prima il valore di temperatura, poi in base ad esso, decidono cosa fare e lo fanno. (heating, cooling, stop)
L'attore User serve per la terza versione e randomicamente richiede un cambio di temperatura al termostato ogni tot secondi.