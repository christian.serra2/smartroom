package user;

import java.time.Duration;
import java.util.Random;
import akka.actor.AbstractActorWithTimers;
import akka.actor.ActorRef;
import message.AchieveTemperature;

public class UserActor extends AbstractActorWithTimers {
	
	private Receive defaultBehaviour;
	Random r = new Random(System.currentTimeMillis());
	
	private class ChangeTemperature {}
	
	public UserActor(ActorRef thermostat){
		defaultBehaviour = receiveBuilder()
				.match(ChangeTemperature.class, msg -> { 
					int i = r.nextInt((40 - 10) + 1) + 10;
					System.out.println("I ask for a change to temperature to "+i+"�");
					thermostat.tell(new AchieveTemperature(i), getSelf());
				})
				.build();
	}
	
	public void preStart() {
		getTimers().startPeriodicTimer("ChangeTemp", new ChangeTemperature(), Duration.ofMillis(10000));
	}	
	
	@Override
	public Receive createReceive() {
		return defaultBehaviour;
	}
}