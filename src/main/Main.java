package main;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import room.RoomTwinActor;
import thermostat.ThermostatActor2;
import user.UserActor;

public class Main {

	public static void main(final String[] args) {
		ActorSystem system = ActorSystem.create("SmartRoom");
		double initialTemperature = 20;
		ActorRef roomTwin = system.actorOf(Props.create(RoomTwinActor.class, initialTemperature));
		//system.actorOf(Props.create(ThermostatActor.class, roomTwin));
		ActorRef thermostat = system.actorOf(Props.create(ThermostatActor2.class, roomTwin));
		system.actorOf(Props.create(UserActor.class, thermostat));
	}
}
