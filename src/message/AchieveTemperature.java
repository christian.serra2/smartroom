package message;

public class AchieveTemperature{
	private double temperature;
	public AchieveTemperature(double temperature) {this.temperature = temperature;}
	public double getTemperatureToAchieve() {return temperature;}
};
