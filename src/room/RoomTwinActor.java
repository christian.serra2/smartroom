package room;

import java.time.Duration;
import java.util.Random;
import akka.actor.AbstractActorWithTimers;
import akka.actor.ActorRef;
import akka.japi.pf.ReceiveBuilder;
import message.AskTemperature;
import message.StartCooling;
import message.StartHeating;
import message.Stop;
import message.TellTemperature;


public class RoomTwinActor extends AbstractActorWithTimers {
	
	
	public class UpdateTemperature {}
	public class DoHeat {}
	public class DoCool {}
	private double temperature;
	private static String UPDATING_TEMPERATURE_TIMING_KEY = "updating";
	private static String HEATING_TIMING_KEY = "heating";
	private static String COOLING_TIMING_KEY = "cooling";
	
	private long updateTemperatureInMs = 2000;
	private long heatingPeriodInMs = 2000;
	private long coolingPeriodInMs = 2000;
	
	private Receive heatingBehaviour;
	private Receive coolingBehaviour;
	private Receive idleBehaviour;
	private ReceiveBuilder defaultBehaviour;
	
	
	public RoomTwinActor(double startingTemperature){
		this.temperature = startingTemperature;
		
		defaultBehaviour = receiveBuilder()
				.match(UpdateTemperature.class, __ -> updateTemperature())
				.match(AskTemperature.class, __ -> tellTemperature(getSender()));
		
		idleBehaviour = defaultBehaviour
				.match(StartHeating.class, __ -> startHeating())
				.match(StartCooling.class, __ -> startCooling())
				.build();
		
		heatingBehaviour = defaultBehaviour
				.match(DoHeat.class, __ -> heat())
				.match(Stop.class, __ ->  stop())
				.match(StartCooling.class, __ -> startCooling())
				.build();
		
		coolingBehaviour = defaultBehaviour
				.match(DoCool.class, __ -> cool())
				.match(Stop.class, __ -> stop())
				.match(StartHeating.class, __ -> startHeating())
				.build();
	}
	
	public void updateTemperature() {
		Random r = new Random();
		double heatingRandomValue = Math.round(r.nextDouble());
		double coolingRandomValue = Math.round(r.nextDouble());
		temperature += heatingRandomValue;
		temperature -= coolingRandomValue;
		System.out.println("Current temperature is " + temperature);
	}
	
	public void preStart() {
		getTimers().startPeriodicTimer(UPDATING_TEMPERATURE_TIMING_KEY, new UpdateTemperature(), 
				Duration.ofMillis(updateTemperatureInMs));
		
	}	
	
	@Override
	public Receive createReceive() {
		return idleBehaviour;
	}
	
	private void startHeating() {
		getTimers().cancel(COOLING_TIMING_KEY);
		getTimers().startPeriodicTimer(HEATING_TIMING_KEY, new DoHeat(), Duration.ofMillis(heatingPeriodInMs));
		getContext().become(heatingBehaviour);
	}
	
	private void heat() {
		temperature += 2;
	}
	
	private void cool() {
		temperature -= 2;
	}
	
	private void startCooling() {
		getTimers().cancel(HEATING_TIMING_KEY);
		getTimers().startPeriodicTimer(COOLING_TIMING_KEY, new DoCool(), Duration.ofMillis(coolingPeriodInMs));
		getContext().become(coolingBehaviour);
	}
	
	private void stop() {
		getTimers().cancel(HEATING_TIMING_KEY);
		getTimers().cancel(COOLING_TIMING_KEY);
		getContext().become(idleBehaviour);
	}
	
	private void tellTemperature(ActorRef inquirer) {
		inquirer.tell(new TellTemperature(temperature), getSelf());
	}
	
	

}