package thermostat;

import akka.actor.AbstractActorWithTimers;
import akka.actor.ActorRef;
import akka.japi.pf.ReceiveBuilder;
import message.AchieveTemperature;
import message.AskTemperature;
import message.StartCooling;
import message.StartHeating;
import message.Stop;
import message.TellTemperature;

public class ThermostatActor extends AbstractActorWithTimers {
	
	private Receive idleBehaviour;
	private ReceiveBuilder defaultBehaviour;
	private double temperature;
	private double temperatureToAchieve;
	private boolean isAchieved = true;
	private boolean isHeating = false;
	private boolean isCooling = false;
	
	public ThermostatActor(ActorRef room){
		defaultBehaviour = receiveBuilder();
		
		idleBehaviour = defaultBehaviour
				.match(AchieveTemperature.class, msg -> {
					temperatureToAchieve = msg.getTemperatureToAchieve();
					System.out.println("We need to achieve "+temperatureToAchieve+"�");
					isAchieved = false;
					room.tell(new AskTemperature(), getSelf());
				})
				.match(TellTemperature.class, msg -> { //Sense, Plan, Act
					temperature = msg.getTemperature();
					if (checkTemperature()) {
						isAchieved = true;
						isCooling = false;
						isHeating = false;
						System.out.println("We achieved the temperature, you can Stop");
						room.tell(new Stop(), getSelf());
					} else {
						if (temperature > temperatureToAchieve) {
							if (isCooling == false) {
								isCooling = true;
								isHeating = false;
								System.out.println("Start cooling please");
								room.tell(new StartCooling(), getSelf());
							}
						} else {
							if (isHeating == false) {
								isHeating = true;
								isCooling = false;
								System.out.println("Start heating please");
								room.tell(new StartHeating(), getSelf());
							}
						}
					}
					if (!isAchieved) {
						room.tell(new AskTemperature(), getSelf());
					} 
				})
				.build();
	}
	
	public void preStart() {
		getSelf().tell(new AchieveTemperature(30), getSelf());
	}	
	
	@Override
	public Receive createReceive() {
		return idleBehaviour;
	}
	
	private boolean checkTemperature() {
		return temperature == temperatureToAchieve;
	}
	
}
